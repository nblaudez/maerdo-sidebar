import { Injectable } from '@angular/core';

@Injectable()
export class MaerdoSidebarService {

  public sidebars;

  constructor() {
    this.sidebars = {
        top: {
          display:false
        },
        bottom: {
          display:false
        },
        left: {
          display:true
        },
        right : {
          display: false
        }
      }

  }

  toggle(sidebar) {
    this.sidebars[sidebar].display = !this.sidebars[sidebar].display;
  }

}
