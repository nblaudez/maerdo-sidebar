import {Component, HostListener, OnInit} from '@angular/core';
import {MaerdoSidebarService} from "./maerdo-sidebar.service";
import {DeviceDetectorService} from "ngx-device-detector";

@Component({
  selector: 'maerdo-sidebars',
  templateUrl: './sidebars.component.html',
  styleUrls: ['./sidebars.component.css']
})
export class MaerdoSidebarComponent implements OnInit {

  protected deviceInfo
  constructor(protected sidebarsService : MaerdoSidebarService,
              protected deviceService: DeviceDetectorService) { }

  ngOnInit() {
    this.deviceInfo = this.deviceService.getDeviceInfo();
  }

  @HostListener('document:mousemove', ['$event'])
  onMouseMove(e) {
    if(window.innerWidth > 768 ) {
      if (e.clientX < 300) {
        this.sidebarsService.sidebars.left.display = true;
      } else {
        this.sidebarsService.sidebars.left.display = false;
      }
    }
  }
}
