import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaerdoSidebarComponent } from './maerdo-sidebar.component';

describe('MaerdoSidebarComponent', () => {
  let component: MaerdoSidebarComponent;
  let fixture: ComponentFixture<MaerdoSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaerdoSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaerdoSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
